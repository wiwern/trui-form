import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  public companyName: string = 'standard-input';
  public nip: string = 'standard-input';
  public email: string = 'standard-input';
  public tel: string = 'standard-input';
  public topic: string = 'standard-input';
  public drivingLic: string = 'standard-input';
  public message: string = 'standard-input';

  constructor() {}

  ngOnInit(): void {
  }

  public applyStyle(style: string): void {
    switch (style) {
      case 'companyName':
        this.companyName = 'standard-input-clicked';
        this.nip = 'standard-input';
        this.topic = 'standard-input';
        this.tel = 'standard-input';
        this.email = 'standard-input';
        this.drivingLic = 'standard-input';
        this.message = 'standard-input';
        break;
      case 'nip':
        this.nip = 'standard-input-clicked';
        this.companyName = 'standard-input';
        this.topic = 'standard-input';
        this.tel = 'standard-input';
        this.email = 'standard-input';
        this.drivingLic = 'standard-input';
        this.message = 'standard-input';
        break;
      case 'email':
        this.email = 'standard-input-clicked';
        this.nip = 'standard-input';
        this.companyName = 'standard-input';
        this.topic = 'standard-input';
        this.tel = 'standard-input';
        this.drivingLic = 'standard-input';
        this.message = 'standard-input';
        break;
      case 'tel':
        this.tel = 'standard-input-clicked';
        this.email = 'standard-input'
        this.nip = 'standard-input';
        this.companyName = 'standard-input';
        this.topic = 'standard-input';
        this.drivingLic = 'standard-input';
        this.message = 'standard-input';
        break;
      case 'drivingLic':
        this.drivingLic = 'standard-input-clicked';
        this.tel = 'standard-input';
        this.email = 'standard-input';
        this.nip = 'standard-input';
        this.companyName = 'standard-input';
        this.topic = 'standard-input';
        this.message = 'standard-input';
        break;
      case 'topic':
        this.topic = 'standard-input-clicked';
        this.nip = 'standard-input';
        this.email = 'standard-input';
        this.tel = 'standard-input';
        this.companyName = 'standard-input';
        this.message = 'standard-input';
        break;
      case 'message':
        this.message = 'standard-input-clicked';
        this.topic = 'standard-input';
        this.nip = 'standard-input';
        this.email = 'standard-input';
        this.tel = 'standard-input';
        this.companyName = 'standard-input';
        break;
      default:
        break;
    }
  }

}
